# PROYECTO DE APLICACIONES EN AMBIENTES LIBRES Sistema de Gestión de Objetos de Aprendizaje

<a href="http://www.epn.edu.ec"><p align="center">
  <img src="https://github.com/kevinjimenez/ProyectoLibres/blob/master/145px-Escudo_de_la_Escuela_Politécnica_Nacional.png"/>
</p></a>

## Objetivo Principal:

* Desarrollar una Aplicación Web que apoye a la construcción, almacenamiento e intercambio de objetos de aprendizaje.

## Objetivos Especificos:

* Estudiar los `Objetos de Aprendizaje`, para determinar sus componentes y las técnicas de construcción.
* Desarrollar una aplicación informática de software libre para la gestión de Objetos de Aprendizaje.
* Desarrollar una aplicación web utilizando herramientas y metodología de desarrollo de software libre, que permita el almacenamiento, catalogación, difusión y uso de `Objetos de Aprendizaje`.

## Resumen:

<p aling="justify">El presente sistema es un recurso lo suficientemente útil para gestionar `Objetos de Aprendizaje`. Su función principal es brindar un confiable repositorio de Objetos de Aprendizaje. Permite Importar, Catalogar y Buscar dichos objetos. El sistema cuenta con gestión de usuarios. Los cuales son: Administrador, Profesores y Estudiantes. La principal función del administrador es gestionar el óptimo funcionamiento del Sistema además conceder permisos a Profesores. Los cuales se registran en el sistema, posteriormente receptan un correo electrónico con sus credenciales, las cuales permiten acceder al sistema con sus respectivos privilegios. Los profesores pueden Importar, Buscar y Comentar objetos. Los estudiantes también se registran en el Sistema y pueden descargar `Objetos de Aprendizaje` para estudiarlos. Finalmente, el sistema provee enlaces a herramientas para apoyar el aprendizaje.
</p>

### Instalacion
* [Manual de Instalación](https://github.com/kevinjimenez/ProyectoLibres/blob/master/Manual%20de%20Instalaci%C3%B3n.docx) - (Enlace Manual de Instalacion)

### Manual de Usuario
* [Manual de Usuario](https://github.com/kevinjimenez/ProyectoLibres/blob/master/Manual%20de%20usuario.docx) - (Enlace Manual de Usuario)

## Integrantes:

* **David Cardenas** - *Desarrollador* - [Davcg5](https://github.com/Davcg5)
* **Kevin Jimenez** - *Desarrollador* - [kevinjimenez](https://github.com/kevinjimenez)
* **Alexis Miranda** - *Desarrollador* - [LexAdrian1](https://github.com/LexAdrian1)

## Desarrollado con:

* [eXeLearning](http://exelearning.net/) - Editor de recursos educativos interactivos gratuito y de código abierto.
* [XAMPP](https://www.apachefriends.org/download.html) - XAMPP es un entorno de desarrollo con PHP.
* [phpMyAdmin](https://www.phpmyadmin.net/) - phpMyAdmin es una herramienta de software libre escrita en PHP, destinada a manejar la administración de MySQL a través de la Web.




